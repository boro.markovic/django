

from django import forms
from .models import Post,Category
choices= Category.objects.all().values_list('name','name')
choice=[]

for item in choices:
    choice.append(item)
class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields= ('title', 'title_tag', 'author' , 'category' , 'body')

        widgets = {
            'title': forms.TextInput(attrs={'class':'text-sm font-medium text-slate-900 border border-black flex '}),
            'title_tag': forms.TextInput(attrs={'class':'text-sm font-medium text-slate-900 border border-black flex form-select mt-1 block w-full '}),
            'author': forms.Select(attrs={'class':'text-sm font-medium text-slate-900 border border-red flex '}),
            'category': forms.Select(choices=choice, attrs={'class':'text-sm font-medium text-slate-900 border border-red flex '}),
            'body': forms.Textarea(attrs={'class':'text-sm font-medium text-slate-900 border border-yellow flex  '}),
        }

class UpdateForm(forms.ModelForm):
    class Meta:
        model = Post
        fields= ('title', 'title_tag','body','category')

        widgets = {
            'title': forms.TextInput(attrs={'class':'text-sm font-medium text-slate-900 border border-black flex '}),
            'title_tag': forms.TextInput(attrs={'class':'text-sm font-medium text-slate-900 border border-black flex form-select mt-1 block w-full '}),
            'body': forms.Textarea(attrs={'class':'text-sm font-medium text-slate-900 border border-yellow flex  '}),
            'category': forms.Select(choices=choice, attrs={'class':'text-sm font-medium text-slate-900 border border-red flex '}),
        }

